﻿using IDP.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace IDP.ResourceClient.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly HttpClient _resourceApiClient;

        public IndexModel(ILogger<IndexModel> logger,
            IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _resourceApiClient = httpClientFactory.CreateClient("resource-api");
        }

        public IEnumerable<WeatherForecast> WeatherForecasts { get; set; }

        public async Task<IActionResult> OnGet()
        {
            await DebugIdentity();

            var responseStream = await _resourceApiClient.GetStreamAsync("/weatherforecast");

            WeatherForecasts = await JsonSerializer.DeserializeAsync<IEnumerable<WeatherForecast>>(responseStream, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });

            return Page();
        }

        private async Task DebugIdentity()
        {
            var idToken = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.IdToken);
            var accessToken = await HttpContext.GetUserAccessTokenAsync();
            var refreshToken = await HttpContext.GetTokenAsync(OpenIdConnectParameterNames.RefreshToken);

            Debug.WriteLine($"AccessToken: {accessToken}");
            Debug.WriteLine($"RefreshToken: {refreshToken}");
            Debug.WriteLine($"IdToken: {idToken}");

            foreach (var claim in User.Claims)
            {
                Debug.WriteLine($"Claim: {claim.Type} - {claim.Value}");
            }
        }
    }
}

﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IDP.IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Address(),
                new IdentityResources.Email(),
            };


        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource("resource_api", "Resource API")
                {
                    Scopes = { "full", "read", "write" }
                }
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("full", "Full access"),
                new ApiScope("read", "Read access"),
                new ApiScope("write", "Write access"),
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client()
                {
                    AllowOfflineAccess = true,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    ClientName = "Resource Client",
                    ClientId = "resource-client-id",
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = new[]
                    {
                        "https://localhost:44359/signin-oidc"
                    },
                    PostLogoutRedirectUris =
                    {
                        "https://localhost:44359/signout-callback-oidc"
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Address,
                        IdentityServerConstants.StandardScopes.Email,
                        "full", "read", "write"
                    },
                    ClientSecrets =
                    {
                        new Secret("resource-client-secret".Sha256())
                    },
                    RequirePkce = true,
                    RequireConsent = true
                }
            };
    }
}